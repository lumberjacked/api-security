<?php
namespace Api\Security\Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * OauthClients
 *
 * @ODM\Document(collection="oauth_clients")
 */
class OauthClients {
    
    public function __construct() {
        $this->redirectUri = '/oauth/recievecode';
    }

    /**
    * @ODM\Id(strategy="AUTO")
    */
    protected $id;

    /**
    * @ODM\Field(name="client_id", type="string")
    */
    protected $clientId;

    /** @ODM\Field(name="client_secret", type="string", nullable=false) */
    protected $clientSecret;

    /** @ODM\Field(name="redirect_uri", type="string", nullable=false) */
    protected $redirectUri;

    /** @ODM\Field(name="grant_type", type="string", nullable=true) */
    protected $grantTypes;

    /** @ODM\Field(name="scope", type="string", nullable=true) */
    protected $scope;

    /** @ODM\Field(name="user_id", type="string", nullable=true) */
    protected $userId;

    public function getClientId() {
        return $this->clientId;
    }

    public function setClientId($clientId) {
        $this->clientId = $clientId;
    }

    public function getClientSecret() {
        return $this->clientSecret;
    }

    public function setClientSecret($secret) {
        $this->clientSecret = (string) $secret;
    }

    public function getRedirectUri() {
        return $this->redirectUri;
    }


    public function setRedirectUri($redirectUri) {
        $this->redirectUri = $redirectUri;
    }

    public function getGrantType() {
        return $this->grantType;
    }

    public function setGrantType($grantType) {
        $this->grantType = $grantType;
    }

    public function getScope() {
        return $this->scope;
    }

    public function setScope($scope) {
        $this->scope = $scope;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }
}