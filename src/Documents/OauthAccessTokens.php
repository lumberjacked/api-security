<?php
namespace Api\Security\Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

 /**
 * OauthAccessTokens
 *
 * @ODM\Document(collection="oauth_access_tokens")
 */
 class OauthAccessTokens {
    
    /** 
    * @ODM\Id(strategy="NONE") 
    * @ODM\Field(name="access_token", type="string")
    */
    protected $accessToken;

    /** @ODM\Field(name="client_id", type="string", length=80, nullable=false) */
    protected $clientId;

    /** @ODM\Field(name="user_id", type="string", nullable=true) */
    protected $userId;

    /** @ODM\Field(name="expires", type="date", nullable=false) */
    protected $expires;

    /** @ODM\Field(name="scope", type="string", length=2000, nullable=true) */
    protected $scope;

    public function getAccessToken() {
        return $this->accessToken;
    }

    public function setAccessToken($accessToken) {
        $this->accessToken = $accessToken; 
    }

    public function getClientId() {
        return $this->clientId;
    }

    public function setClientId($clientId) {
        $this->clientId = $clientId;
    }

    public function getUserId() {
        return $this->userId;
    }


    public function setUserId($userId) {
        $this->userId = $userId;
    }

    public function getExpires() {
        return $this->expires;
    }

    public function setExpires($expires) {
        $this->expires = $expires;
    }

    public function getScope() {
        return $this->scope;
    }

    public function setScope($scope) {
        $this->scope = $scope;
    }
}