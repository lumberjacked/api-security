<?php
namespace Api\Security\Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * OauthRefreshTokens
 *
 * @ODM\Document(collection="oauth_refresh_tokens")
 */
class OauthRefreshTokens
{

    /**
     * @var integer
     *
     * @ODM\Id(strategy="AUTO")
     * @ODM\Field(name="id", type="increment")
     */
    public $id;

    /**
     * @var integer
     *
     * @ODM\Field(name="refresh_token", type="string", length=40, nullable=false)
     */
    private $refreshToken;

    /**
     * @var string
     *
     * @ODM\Field(name="client_id", type="string", length=80, nullable=false)
     */
    private $clientId;

    /**
     * @var string
     *
     * @ODM\Field(name="user_id", type="string", length=255, nullable=true)
     */
    private $userId;

    /**
     * @var string
     *
     * @ODM\Field(name="expires", type="date", nullable=false)
     */
    private $expires;

    /**
     * @var string
     *
     * @ODM\Field(name="scope", type="string", length=2000, nullable=true)
     */
    private $scope;

}