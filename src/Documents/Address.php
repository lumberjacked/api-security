<?php
namespace Api\Security\Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Address
 * @ODM\EmbeddedDocument
 */
class Address {

    /** @ODM\Field(type="string") */
    protected $street;
 
    /** @ODM\Field(type="string") */
    protected $city;

    /** @ODM\Field(type="string") */
    protected $state;

    /** @ODM\Field(type="string") */
    protected $zip;

    public function setStreet($street) {
        $this->street = $street;
    }

    public function getStreet() {
        return $this->street;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function getCity() {
        return $this->city;
    }

    public function setState($state) {
        $this->state = $state;
    }

    public function getState() {
        return $this->state;
    }

    public function setZip($zip) {
        $this->zip = $zip;
    }

    public function getZip() {
        return $this->zip;
    }

}