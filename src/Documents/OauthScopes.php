<?php
namespace Api\Security\Documents;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * OauthScopes
 *
 * @ODM\Document(collection="oauth_scopes")
 */
class OauthScopes {
    
    /**
    * @ODM\Id 
    * @ODM\Field(name="client_id", type="string", length=80)
    */
    protected $clientId;

    /** @ODM\Field(name="type", type="string", nullable=false) */
    protected $type;

    /** @ODM\Field(name="scope", type="string", length=2000) */
    protected $scope = 'supported';

    /** @ODM\Field(name="is_default", type="increment") */
    protected $is_default;

    public function getClientId() {
        return $this->clientId;
    }

    public function setClientId($clientId) {
        $this->clientId = $clientId;
    }

    public function getType() {
        return $this->clientId;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getScope() {
        return $this->scope;
    }

    public function setScope($scope) {
        $this->scope = $scope;
    }

    public function getIsDefault() {
        return $this->is_default;
    }

    public function setIsDefault($is_default) {
        $this->is_default = $is_default;
    }
}