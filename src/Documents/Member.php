<?php
namespace Api\Security\Documents;

use Api\Security\Documents\Address;
use Zend\Stdlib\Hydrator\ClassMethods;
use ZfcRbac\Identity\IdentityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Member
 *
 * @ODM\Document(collection="members")
 */
class Member implements IdentityInterface {

    /** @ODM\Id(strategy="AUTO") */
    protected $id;

    /** @ODM\Field(type="string") */
    protected $username;

    /** @ODM\Field(type="hash") */
    protected $fullName;

    /** @ODM\Field(type="collection") */
    protected $phones;

    /** @ODM\EmbedMany(targetDocument="Address") */
    protected $address;

    /** @ODM\Field(type="string") */
    protected $avatar;

    /** @ODM\Field(type="string") */
    protected $password;

    /** @ODM\Field(type="collection") */
    protected $roles;

    /** @ODM\Field(type="date") */
    protected $memberSince;
 
    public function __construct() {
        $this->fullName = array("firstname" => "", "lastname" => "", "nickname" => "");
        $this->phones   = array();
        $this->address  = new ArrayCollection;
        $this->roles    = array();    
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setFirstname($first) {
        $this->fullName['firstname'] = $first;
    }

    public function setLastname($last) {
        $this->fullName['lastname'] = $last;
    }

    public function setNickname($nick) {
        $this->fullName['nickname'] = $nick;
    }

    public function getFullname() {
        return $this->fullName;
    }

    public function setPhones(array $phones) {
        $this->phones = $phones;
    }

    public function getPhones() {
        return $this->phones;
    }

    public function setAddress(Address $address) {
        $this->address->add($address);
    }

    public function getAddresses() {
        
        $hydrator = new ClassMethods;
        $results = array();
        foreach($this->address->toArray() as $i => $address) {
            $results[] = $hydrator->extract($address);   
        }
        
        return $results;
    }

    public function setAvatar($avatar) {
        $this->avatar = $avatar;
    }

    public function getAvatar() {
        return $this->avatar;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setRole($role) {
        $this->roles[] = $role;
    }

    public function setRoles($roles)
    {
        if (!is_array($roles)) {
            $roles = array($roles);
        }
        $this->roles = $roles;
        return $this;
    }

    /**
     * Get the list of roles of this identity
     *
     * @return string[]|\Rbac\Role\RoleInterface[]
     */
    public function getRoles()
    {
        return $this->roles;
    }

    public function setMemberSince($date) {
        $this->memberSince = $date;
    }
}