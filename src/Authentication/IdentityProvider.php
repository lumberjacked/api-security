<?php

namespace Api\Security\Authentication;

use Cwg\Admin\Authentication\Identity;
use Zend\ServiceManager\ServiceManager;
use Doctrine\ODM\MongoDB\DocumentManager;
use Zend\Authentication\AuthenticationService;
use ZfcRbac\Identity\IdentityProviderInterface;

/**
 * Class IdentityProvider provides Identity object required by RBAC.
 * We return custom Identity because we connect OAuth2 authentication (returning userId) and RBAC authorization (requiring roles)
 *
 * @package Cwg\Admin\Authentication
 */
class IdentityProvider implements IdentityProviderInterface
{
    /** @var Identity $rbacIdentity */
    private $rbacIdentity = null;

    /* @var \Zend\Authentication\AuthenticationService $authenticationProvider */
    private $authenticationProvider;

    /* @var \Doctrine\ODM\MongoDB\DocumentManager */
    private $doctrine;

    private $services;

    public function setDoctrineDocumentManager(DocumentManager $doctrine) {
        $this->doctrine = $doctrine;
    }

    public function setAuthenticationProvider(AuthenticationService $authenticationProvider)
    {
        $this->authenticationProvider = $authenticationProvider;
        return $this;
    }

    public function setServiceManager(ServiceManager $services) {
        $this->services = $services;
    }

    /**
     * Checks if user is authenticated. If yes, checks db for user's role and returns Identity.
     *
     * @return Identity
     */
    public function getIdentity()
    {
        if ($this->rbacIdentity === null)
        {
            $mvcIdentity = $this->authenticationProvider->getIdentity();
            $identityClass = get_class($this->services->get('identity'));
            
            $this->rbacIdentity = $this->doctrine->getRepository($identityClass)->findOneBy(array('username' => $mvcIdentity->getRoleId()));
        }
        
        return $this->rbacIdentity;
    }
}