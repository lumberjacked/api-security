<?php

namespace Api\Security\Authentication\Factories;

use \Zend\ServiceManager\ServiceManager;
use Api\Security\Authentication\IdentityProvider;

class IdentityProviderFactory
{
    public function __invoke(ServiceManager $services)
    {
        /** @var \Zend\Authentication\AuthenticationService $authenticationProvider */
        $authenticationProvider = $services->get('authentication');
        
        $identityProvider = new IdentityProvider();
        $identityProvider->setAuthenticationProvider($authenticationProvider);
        $identityProvider->setDoctrineDocumentManager($services->get('doctrine.documentmanager.odm_default'));
        $identityProvider->setServiceManager($services);
        return $identityProvider;
    }
}